# Revolut

## Functionality 
The application loads the data from https://revolut.duckdns.org/latest?base=EUR every second.
When the user clicks on a currency from the list it becomes active and the user can change the amount. 
The amount of the rest of the items are recalculated based on the amount of the selected item and the difference in the rates.


## Used technology

### Application
1. `Kotlin` as a language.
2. `Koin` as a Dependency Injection framework - it allows to inject different implementation of the repository for the Espresso tests.
3. `Retrofit` for network calls.
4. `RxJava` to easily chain operation.
5. `Picasso` for loading images.
6. `ViewModel` as a presentation layer.
7. `LiveData` to transmit the data to the view (in that case to the activity).
8. Animated drawable - I created an animated vector drawable and used it instead of a loading indicator.
9. `RecyclerView` to display the list
10. `DiffUtils` to find the differences in the list in order the Recycler View to be able to animate the item changes.

### Unit Test
I created unit test that is testing and covering 100% of `MainActivityViewModel`.

### UI Test
1. `Espresso`
2. `Kakao` on top of `Espresso` is making the UI testing nicer.
3. I created `TestCurrencyRatesRepository` that is implementing the repository interface. During the `MainActivityTest` 
startup I'm replacing with the help of Koin the implementation in the ViewModel.
This allow to test the UI without any dependency to the Network or server.

## Videos
1. [Basic application usage](https://bitbucket.org/yanslavm/revolut/raw/ba5de50d8bc4d1e1e0c36fe7d358b000df39a170/media/device-using-app.mp4)
2. [UI Tests execution](https://bitbucket.org/yanslavm/revolut/raw/ba5de50d8bc4d1e1e0c36fe7d358b000df39a170/media/device-ui-tests.mp4)

## Image from the application

![App screen](https://bitbucket.org/yanslavm/revolut/raw/28d2f0f5b463b8112de1d800d992353290d56341/media/main-screen.png | width=100)
package com.yanislav.revolut.viewmodel

import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.*
import com.yanislav.revolut.model.Currency
import com.yanislav.revolut.model.LatestRatesResponse
import com.yanislav.revolut.model.NetworkState
import com.yanislav.revolut.repository.CurrencyRatesRepository
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import rx.Observable
import java.io.IOException
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import rx.schedulers.TestScheduler



class MainActivityViewModelTest {

    private val networkSateLiveData: MutableLiveData<NetworkState> = mock()
    private val ratesLiveData: MutableLiveData<ArrayList<Currency>> = mock()
    private val currencyRatesRepository: CurrencyRatesRepository = mock()

    private val testObservable: Observable<Long> = Observable.interval(0, 1, TimeUnit.MILLISECONDS)
    private val loadingImageObservable: Observable<Long> = Observable.timer(2, TimeUnit.SECONDS)

    private val testRates =  mapOf("GBP" to 1.50, "BGN" to 2.00)

    private lateinit var viewModel: MainActivityViewModel

    @Before
    fun setUp() {
        viewModel = getViewModel(testObservable, loadingImageObservable)
    }

    @Test
    fun getNetworkStateLiveData() {
        assertEquals(networkSateLiveData, viewModel.networkStateLiveData)
    }

    @Test
    fun getRatesLiveData() {
        assertEquals(ratesLiveData, viewModel.ratesLiveData)
    }

    @Test
    fun getBaseCurrency_initial_returnEuro() {
        assertEquals("EUR", viewModel.baseCurrency.key)
    }

    @Test
    fun getBaseCurrency_updateCurrencyWithoutOldData_returnNewCurrencyWithoutRatesUpdate() {
        viewModel.baseCurrency = Currency("BGN", 200.00, true)
        assertEquals("BGN", viewModel.baseCurrency.key)

        verify(ratesLiveData).value
        verifyNoMoreInteractions(ratesLiveData)
    }

    @Test
    fun setBaseCurrency_hasOldData_updateList() {
        val currency = Currency("BGN", 200.00, false)
        val arrayList = ArrayList<Currency>()
        arrayList.add(currency)
        whenever(ratesLiveData.value).thenReturn(arrayList)

        viewModel.baseCurrency = currency

        assertTrue(currency.isBase)

        verify(ratesLiveData).value
        verify(ratesLiveData).postValue(arrayList)
    }

    @Test
    fun getBaseCurrencyAmount_initial_equalsToCurrency() {
        assertEquals(viewModel.baseCurrency.amount, viewModel.baseCurrencyAmount, 0.00)
    }

    @Test
    fun setBaseCurrencyAmount_basicUpdate_checkValue() {
        viewModel.baseCurrencyAmount = 300.00
        assertEquals(300.00, viewModel.baseCurrencyAmount, 0.00)
    }

    @Test
    fun setBaseCurrencyAmount_hasLoadedData_recalculateData() {
        testSuccessfullyLoadedData()
        reset(ratesLiveData)
        viewModel.baseCurrencyAmount = 200.00

        verifyRatesValues(mapOf(
            "BGN" to 400.00,
            "GBP" to 300.00,
            "EUR" to 200.00))
    }

    @Test
    fun setBaseCurrencyAmount_afterBaseCurrencyIsChanged_recalculateData() {
        testSuccessfullyLoadedData()
        argumentCaptor<ArrayList<Currency>>().apply {
            verify(ratesLiveData).postValue(capture())

            val resultMap = firstValue.map { it.key to it }.toMap()

            viewModel.baseCurrency = resultMap["BGN"]!!
        }

        reset(ratesLiveData)

        viewModel.baseCurrencyAmount = 200.00

        verifyRatesValues(mapOf(
            "BGN" to 200.00,
            "GBP" to 150.00,
            "EUR" to 100.00))
    }

    private fun verifyRatesValues(expectedResultMap: Map<String, Double>) {
        argumentCaptor<ArrayList<Currency>>().apply {
            verify(ratesLiveData).postValue(capture())

            val resultMap = firstValue.map { it.key to it.amount }.toMap()

            for (entry in expectedResultMap.entries) {
                assertEquals(entry.value, resultMap[entry.key])
            }
        }
    }

    @Test
    fun onResume_success() {
        testSuccessfullyLoadedData()
    }

    @Test
    fun onResume_onError_notifyError() {
        reset(networkSateLiveData)
        viewModel = getViewModel(Observable.just(1), Observable.just(1))

        val repositoryObservable = Observable.error<LatestRatesResponse>(IOException())
        whenever(currencyRatesRepository.getLatestRatesObservable("EUR"))
            .thenReturn(repositoryObservable)

        viewModel.onResume()

        verify(currencyRatesRepository).getLatestRatesObservable("EUR")
        verify(networkSateLiveData).postValue(NetworkState.LOADING)
        verify(networkSateLiveData).postValue(NetworkState.ERROR)
        verifyNoMoreInteractions(ratesLiveData)
    }

    @Test
    fun onResume_errorAfterSuccess_noSecondNotification() {
        reset(networkSateLiveData)
        viewModel = getViewModel(Observable.just(1), Observable.just(1))

        val repositoryObservable = Observable.error<LatestRatesResponse>(IOException())
        whenever(currencyRatesRepository.getLatestRatesObservable("EUR"))
            .thenReturn(repositoryObservable)

        whenever(networkSateLiveData.value).thenReturn(NetworkState.SUCCESS)

        viewModel.onResume()

        verify(networkSateLiveData).postValue(NetworkState.LOADING)
        verify(networkSateLiveData).value
        verifyNoMoreInteractions(networkSateLiveData)
    }

    private fun getViewModel(requestIntervalObservable: Observable<Long>,
                             loadingImageIntervalObservable: Observable<Long>): MainActivityViewModel {
        return MainActivityViewModel(
            currencyRatesRepository,
            networkSateLiveData,
            ratesLiveData,
            requestIntervalObservable,
            loadingImageIntervalObservable
        )
    }

    @Test
    fun onPause_withoutStarting_noException() {
        viewModel.onPause()
    }

    @Test
    fun onPause_firstStart_verifyCallsAreCanceled() {
        viewModel.onResume()

        viewModel.onPause()

        assertEquals(0, viewModel.ratesUpdateSubscription.size)
    }

    private fun testSuccessfullyLoadedData() {
        reset(networkSateLiveData)

        viewModel = getViewModel(Observable.just(1), loadingImageObservable)

        val rates = HashMap<String, Double>(testRates)
        val result = LatestRatesResponse("EUR", Date(), rates)
        val repositoryObservable = Observable.just(result)
        whenever(currencyRatesRepository.getLatestRatesObservable("EUR"))
            .thenReturn(repositoryObservable)

        viewModel.onResume()

        repositoryObservable.test().assertCompleted()

        verify(currencyRatesRepository).getLatestRatesObservable("EUR")
        verify(networkSateLiveData).postValue(NetworkState.LOADING)
        verify(ratesLiveData).postValue(any())
        verifyNoMoreInteractions(networkSateLiveData)
    }
}
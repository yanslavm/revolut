package com.yanislav.revolut

import androidx.lifecycle.MutableLiveData
import androidx.test.rule.ActivityTestRule
import com.yanislav.revolut.model.LatestRatesResponse
import com.yanislav.revolut.screen.MainScreen
import com.yanislav.revolut.stub.TestCurrencyRatesRepository
import com.yanislav.revolut.stub.TestCurrencyRatesResult
import com.yanislav.revolut.ui.activity.MainActivity
import com.yanislav.revolut.util.waitForObject
import com.yanislav.revolut.viewmodel.MainActivityViewModel
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext.loadKoinModules
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.test.KoinTest
import rx.Observable
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap

class MainActivityTest: KoinTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java, true, false)

    val screen = MainScreen()

    private val mockRatesRepository = TestCurrencyRatesRepository()
    private val latestRatesResponse = LatestRatesResponse("EUR", Date(), HashMap(mapOf("BGN" to 2.00, "GBP" to 1.5)))

    @Before
    fun setUp() {
        loadKoinModules(
            module {
                viewModel { MainActivityViewModel(mockRatesRepository, MutableLiveData(), MutableLiveData(),
                    Observable.interval(0, MainActivityViewModel.REQUEST_INTERVAL_IN_SECONDS, TimeUnit.SECONDS),
                    Observable.just(1)) }
            }
        )

        rule.launchActivity(null)
    }

    @After
    fun cleanUp() {
        stopKoin()
    }

    @Test
    fun successfullyLoaded_selectLastItem() {
        mockRatesRepository.addExpectedResult(TestCurrencyRatesResult(result = latestRatesResponse))

        waitForObject(R.id.rates_list)

        screen {
            ratesList {
                isVisible()
                hasSize(3)

                firstChild<MainScreen.Item> {
                    title { hasText("EUR") }
                    amount { hasText( "1.00")}
                }

                lastChild<MainScreen.Item> {
                    title { hasText( "GBP")}
                    amount { hasText( "1.50")}
                    click()
                }

                firstChild<MainScreen.Item> {
                    title { hasText("GBP") }
                    amount { hasText( "1.50")}
                }
            }
        }
    }

    @Test
    fun editFirstItem_verifySecondItemAmountIsUpdated() {
        mockRatesRepository.addExpectedResult(TestCurrencyRatesResult(result = latestRatesResponse))

        waitForObject(R.id.rates_list)

        screen {
            ratesList {
                isVisible()
                hasSize(3)

                firstChild<MainScreen.Item> {
                    title { hasText("EUR") }
                    amount {
                        isNotFocused()
                        click()
                        isFocused()
                        clearText()
                        typeText("2.00")
                    }
                }

                childAt<MainScreen.Item>(1) {
                    title { hasText( "BGN")}
                    amount { hasText( "4.00")}
                }

                lastChild<MainScreen.Item> {
                    title { hasText( "GBP")}
                    amount { hasText( "3.00")}
                }
            }
        }
    }

    @Test
    fun clearFirstItem_verifySecondItemAmountIsUpdated() {
        mockRatesRepository.addExpectedResult(TestCurrencyRatesResult(result = latestRatesResponse))

        waitForObject(R.id.rates_list)

        screen {
            ratesList {
                isVisible()
                hasSize(3)

                firstChild<MainScreen.Item> {
                    title { hasText("EUR") }
                    amount {
                        isNotFocused()
                        click()
                        isFocused()
                        clearText()
                    }
                }

                childAt<MainScreen.Item>(1) {
                    title { hasText( "BGN")}
                    amount { hasText( "0.00")}
                }

                lastChild<MainScreen.Item> {
                    title { hasText( "GBP")}
                    amount { hasText( "0.00")}
                }
            }
        }
    }

    @Test
    fun selectSecondItemThenSelectThirdItem_makeSureItemIsSelected() {
        mockRatesRepository.addExpectedResult(TestCurrencyRatesResult(result = latestRatesResponse))

        waitForObject(R.id.rates_list)

        screen {
            ratesList {
                isVisible()
                hasSize(3)

                childAt<MainScreen.Item>(1) {
                    title { hasText( "BGN")}
                    click()
                }

                firstChild<MainScreen.Item> {
                    title { hasText("BGN")}
                    amount {hasText("2.00")}
                }

                lastChild<MainScreen.Item> {
                    title { hasText( "GBP")}
                    amount { hasText( "1.50")}
                    click()
                }

                firstChild<MainScreen.Item> {
                    title { hasText( "GBP")}
                    amount { hasText( "1.50")}
                }
            }
        }
    }
}
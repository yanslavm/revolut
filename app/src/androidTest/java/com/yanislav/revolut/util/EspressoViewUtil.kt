package com.yanislav.revolut.util

import android.os.SystemClock
import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.Matcher

fun waitForObject(resId: Int): Boolean {
    return checkIfElementIsDisplayed(withText(resId)) || checkIfElementIsDisplayed(withId(resId))
}

fun checkIfElementIsDisplayed(matcher: Matcher<View>): Boolean {
    for (i in 0 until 100) {
        try {
            onView(matcher).check(matches(isDisplayed()))
            return true
        } catch (e: NoMatchingViewException) {
            e.printStackTrace()
            SystemClock.sleep(2L)
        }

    }
    return false
}
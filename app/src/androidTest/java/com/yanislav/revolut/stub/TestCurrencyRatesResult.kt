package com.yanislav.revolut.stub

import com.yanislav.revolut.model.LatestRatesResponse
import java.lang.Exception

data class TestCurrencyRatesResult(
    val result: LatestRatesResponse? = null,
    val exception: Exception? = null,
    val timeoutMs: Long = 10)
package com.yanislav.revolut.stub

import com.yanislav.revolut.model.LatestRatesResponse
import com.yanislav.revolut.repository.CurrencyRatesRepository
import rx.Observable
import java.io.IOException
import java.util.*

/**
 * Mock version of {@link CurrencyRatesRepository.
 */
class TestCurrencyRatesRepository: CurrencyRatesRepository {
    private val requests: Queue<TestCurrencyRatesResult> = LinkedList<TestCurrencyRatesResult>()
    override fun getLatestRatesObservable(baseCurrency: String): Observable<LatestRatesResponse> {
        val request = requests.poll()
        request?.let {
            return if (it.result != null) {
                Observable.just(it.result)
            } else {
                Observable.error(it.exception)
            }
        }

        return Observable.error(IOException("Missing requests"))
    }

    fun addExpectedResult(testResult: TestCurrencyRatesResult) {
        requests.offer(testResult)
    }
}
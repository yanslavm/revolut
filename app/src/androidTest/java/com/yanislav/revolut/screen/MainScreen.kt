package com.yanislav.revolut.screen

import android.view.View
import com.agoda.kakao.*
import com.yanislav.revolut.R
import org.hamcrest.Matcher

class MainScreen: Screen<MainScreen>() {
    val loadingImage = KImageView {withId(R.id.loading_image)}
    val ratesList: KRecyclerView = KRecyclerView({
        withId(R.id.rates_list)
    }, {
        itemType(::Item)

    })

    class Item(parent: Matcher<View>) : KRecyclerItem<Item>(parent) {
        val title: KTextView = KTextView(parent) { withId(R.id.item_currency_name) }
        val description: KTextView = KTextView(parent) { withId(R.id.item_currency_description) }
        val amount: KEditText = KEditText(parent) { withId(R.id.item_currency_amount) }
    }
}
package com.yanislav.revolut.model

import com.google.gson.annotations.SerializedName
import java.util.*

data class LatestRatesResponse(
    @SerializedName("base") val base: String,
    @SerializedName("date") val date: Date,
    @SerializedName("rates") val rates: HashMap<String, Double>)
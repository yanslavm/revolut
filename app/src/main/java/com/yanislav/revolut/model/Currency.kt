package com.yanislav.revolut.model

data class Currency(
    val key: String,
    var amount: Double,
    var isBase: Boolean,
    var isEditable: Boolean = false)

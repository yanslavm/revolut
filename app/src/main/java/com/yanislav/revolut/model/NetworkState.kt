package com.yanislav.revolut.model

enum class NetworkState {
    LOADING,
    SUCCESS,
    ERROR
}

package com.yanislav.revolut.adapter.viewholder

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yanislav.revolut.R
import com.yanislav.revolut.adapter.callback.CurrenciesRatesAdapterCallback
import com.yanislav.revolut.extension.format
import com.yanislav.revolut.extension.load
import com.yanislav.revolut.model.Currency
import com.yanislav.revolut.util.getCurrencyDescription

abstract class BaseViewHolder(itemView: View, private val callback: CurrenciesRatesAdapterCallback):
        RecyclerView.ViewHolder(itemView) {
    private val currencyName: TextView = itemView.findViewById(R.id.item_currency_name)
    private val currencyDescription: TextView = itemView.findViewById(R.id.item_currency_description)
    private val currencyImage: ImageView = itemView.findViewById(R.id.item_image_view)
    protected lateinit var currentCurrency: Currency
    private var currentPosition: Int = -1

    init {
        itemView.setOnClickListener{
            onItemClicked()
        }
    }

    protected fun onItemClicked() {
        callback.onCurrencyClicked(currentCurrency, currentPosition)
    }

    fun setAmount(amount: Double) {
        setAmount(amount.format(2))
    }

    abstract fun setAmount(amountStr: String)

    open fun setItem(currency: Currency, position: Int) {
        currentCurrency = currency
        currentPosition = position
        currencyName.text = currency.key
        currencyDescription.text = getCurrencyDescription(itemView.context, currency.key)
        currencyImage.load(currency.key)

        setAmount(currency.amount)
    }
}
package com.yanislav.revolut.adapter.callback

import com.yanislav.revolut.model.Currency

interface CurrenciesRatesAdapterCallback {
    /**
     * Callback that is triggered when the user selects a new base currency.
     */
    fun onCurrencyClicked(currency: Currency, position: Int)

    /**
     * Callback that is triggered when the user changes the amount of the current base currency.
     */
    fun onCurrencyAmountChanged(amount: Double)
}
package com.yanislav.revolut.adapter.viewholder

import android.view.View
import android.widget.TextView
import com.yanislav.revolut.R
import com.yanislav.revolut.adapter.callback.CurrenciesRatesAdapterCallback

class OtherItemViewHolder(itemView: View, callback: CurrenciesRatesAdapterCallback): BaseViewHolder(itemView, callback) {
    private val currencyAmount: TextView = itemView.findViewById(R.id.item_currency_amount)

    init {
        currencyAmount.setOnClickListener { onItemClicked() }
    }

    override fun setAmount(amountStr: String) {
        currencyAmount.text = amountStr
    }
}
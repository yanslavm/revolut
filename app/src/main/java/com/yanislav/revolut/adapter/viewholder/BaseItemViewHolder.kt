package com.yanislav.revolut.adapter.viewholder

import android.view.View
import android.widget.EditText
import com.yanislav.revolut.R
import com.yanislav.revolut.adapter.callback.CurrenciesRatesAdapterCallback
import com.yanislav.revolut.extension.afterTextChanged
import com.yanislav.revolut.extension.showKeyBoard
import com.yanislav.revolut.extension.toDouble

class BaseItemViewHolder(itemView: View, callback: CurrenciesRatesAdapterCallback): BaseViewHolder(itemView, callback) {

    private val currencyAmount: EditText = itemView.findViewById(R.id.item_currency_amount)

    init {
        currencyAmount.afterTextChanged {
            callback.onCurrencyAmountChanged(currencyAmount.toDouble())
        }
    }

    override fun setAmount(amountStr: String) {
        currencyAmount.setText(amountStr)

        if (currentCurrency.isEditable) {
            // When the list is shown for a first time we don't want to force it to focus the first element.
            // We want to do it only when the user explicitly selects an item
            currencyAmount.showKeyBoard()
        }
    }
}
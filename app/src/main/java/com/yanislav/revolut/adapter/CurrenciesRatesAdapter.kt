package com.yanislav.revolut.adapter

import android.os.Bundle
import android.os.Handler
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.yanislav.revolut.R
import com.yanislav.revolut.adapter.callback.CurrenciesRatesAdapterCallback
import com.yanislav.revolut.adapter.viewholder.BaseItemViewHolder
import com.yanislav.revolut.adapter.viewholder.BaseViewHolder
import com.yanislav.revolut.adapter.viewholder.OtherItemViewHolder
import com.yanislav.revolut.model.Currency
import com.yanislav.revolut.extension.inflate
import com.yanislav.revolut.util.DiffUtilCallback


class CurrenciesRatesAdapter(private val callback: CurrenciesRatesAdapterCallback): RecyclerView.Adapter<BaseViewHolder>() {

    private enum class CurrencyViewType {
        BASE_ITEM,
        OTHER
    }

    private val items = ArrayList<Currency>()

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        return when (item.isBase) {
            true -> CurrencyViewType.BASE_ITEM.ordinal
            false -> CurrencyViewType.OTHER.ordinal
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        return when (viewType) {
            CurrencyViewType.BASE_ITEM.ordinal -> {
                BaseItemViewHolder(parent.inflate(R.layout.item_base_rate, false), callback)
            }
            else ->  {
                OtherItemViewHolder(parent.inflate(R.layout.item_other_rates, false), callback)
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.setItem(items[position], position)
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int, payloads: MutableList<Any>) {
        if (payloads.isEmpty()) {
            super.onBindViewHolder(holder, position, payloads)
        } else {
            val bundle = payloads[0] as Bundle
            for (key in  bundle.keySet()) {
                when (key) {
                    DiffUtilCallback.FIELD_KEY -> super.onBindViewHolder(holder, position, payloads)
                    DiffUtilCallback.FIELD_AMOUNT ->
                        if (position > 0) {
                            // We don't want to update the amount of the base item (the one that we are editing)
                            // as it will jump the cursor to the beginning of the string in the Edit text
                            // and will disturb the editing
                            holder.setAmount(bundle.getDouble(key))
                        }
                }
            }
        }
    }

    fun updateList(list: ArrayList<Currency>) {
        updateItemsInternal(list)
    }

    /**
     * Calls the DiffUtil on a backgroud thread and once it is ready calls {@link applyDiffResult} with the result.
     */
    private fun updateItemsInternal(newItems: List<Currency>) {
        val oldItems = ArrayList(this.items)
        val handler = Handler()
        Thread(Runnable {
            val diffResult = DiffUtil.calculateDiff(DiffUtilCallback(oldItems, newItems))
            handler.post { applyDiffResult(newItems, diffResult) }
        }).start()
    }

    private fun applyDiffResult(newItems: List<Currency>, diffResult: DiffUtil.DiffResult) {
        items.clear()
        items.addAll(newItems)
        diffResult.dispatchUpdatesTo(this)
    }
}
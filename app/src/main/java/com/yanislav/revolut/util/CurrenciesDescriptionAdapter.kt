package com.yanislav.revolut.util

import android.content.Context
import com.yanislav.revolut.R

private val currencyDescriptionMap = mapOf(
    "AUD" to R.string.currency_aud,
    "BGN" to R.string.currency_bgn,
    "BRL" to R.string.currency_brl,
    "CAD" to R.string.currency_cad,
    "CHF" to R.string.currency_chf,
    "CNY" to R.string.currency_cny,
    "CZK" to R.string.currency_czk,
    "DKK" to R.string.currency_dkk,
    "EUR" to R.string.currency_eur,
    "GBP" to R.string.currency_gbp,
    "HKD" to R.string.currency_hkd,
    "HRK" to R.string.currency_hrk,
    "HUF" to R.string.currency_huf,
    "IDR" to R.string.currency_idr,
    "ILS" to R.string.currency_ils,
    "INR" to R.string.currency_inr,
    "ISK" to R.string.currency_isk,
    "JPY" to R.string.currency_jpy,
    "KRW" to R.string.currency_krw,
    "MXN" to R.string.currency_mxn,
    "MYR" to R.string.currency_myr,
    "NOK" to R.string.currency_nok,
    "NZD" to R.string.currency_nzd,
    "PHP" to R.string.currency_php,
    "PLN" to R.string.currency_pln,
    "RON" to R.string.currency_ron,
    "RUB" to R.string.currency_rub,
    "SEK" to R.string.currency_sek,
    "SGD" to R.string.currency_sgd,
    "THB" to R.string.currency_thb,
    "TRY" to R.string.currency_try,
    "USD" to R.string.currency_usd,
    "ZAR" to R.string.currency_zar)

/**
 * Gets the description of the currency or null.
 */
fun getCurrencyDescription(context: Context, currencyKey: String): String? {
    val resId = currencyDescriptionMap[currencyKey]
    return if (resId != null) {
        context.getString(resId)
    } else {
        null
    }
}
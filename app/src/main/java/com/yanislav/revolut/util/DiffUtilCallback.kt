package com.yanislav.revolut.util

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.yanislav.revolut.model.Currency


class DiffUtilCallback(private val oldList: List<Currency>, private val newList: List<Currency>): DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].key == newList[newItemPosition].key &&
                oldList[oldItemPosition].amount == newList[newItemPosition].amount
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].key == newList[newItemPosition].key
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val newContact = newList[newItemPosition]
        val oldContact = oldList[oldItemPosition]

        val diff = Bundle()
        if (newContact.key != oldContact.key) {
            diff.putString(FIELD_KEY, newContact.key)
        }

        if (newContact.amount != oldContact.amount) {
            diff.putDouble(FIELD_AMOUNT, newContact.amount)
        }
        return if (diff.size() == 0) {
            null
        } else diff
    }

    companion object {
        const val FIELD_KEY = "key"
        const val FIELD_AMOUNT = "amount"
    }
}
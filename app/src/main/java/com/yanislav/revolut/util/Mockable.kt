package com.yanislav.revolut.util

/**
 * Annotation that is used to allow us to mock the final Kotlin classes.
 * If you need a class to be mocked just annotate it with this annotation.
 */
annotation class Mockable
package com.yanislav.revolut.util

import rx.Scheduler

interface RxSchedulers {
    fun io(): Scheduler
}

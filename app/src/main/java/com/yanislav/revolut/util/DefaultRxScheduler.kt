package com.yanislav.revolut.util

import rx.Scheduler
import rx.schedulers.Schedulers

class DefaultRxScheduler : RxSchedulers {
    override fun io(): Scheduler {
        return Schedulers.io()
    }
}

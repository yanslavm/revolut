package com.yanislav.revolut.viewmodel

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.yanislav.revolut.model.Currency
import com.yanislav.revolut.model.LatestRatesResponse
import com.yanislav.revolut.model.NetworkState
import com.yanislav.revolut.repository.CurrencyRatesRepository
import rx.Observable
import rx.Subscription
import java.util.concurrent.atomic.AtomicBoolean


class MainActivityViewModel(
    private val currencyRatesRepository: CurrencyRatesRepository,
    /**
     * LiveData that provides updates regarding the current communication state.
     */
    val networkStateLiveData: MutableLiveData<NetworkState>,

    /**
     * Live Data that provides the latest list with currency rates.
     */
    val ratesLiveData: MutableLiveData<ArrayList<Currency>>,

    /**
     * Observable that is emitting events on a certain period of time in order to retrigger the network calls.
     */
    private val requestIntervalObservable: Observable<Long>,

    /**
     * Observable that will emit just one event after a certain time that will allow the loading image/indicator
     * to be displayed for a minimum perioud of time.
     */
    private val minimumLoadingImageTimerObservable: Observable<Long>): ViewModel(){

    /**
     * The currently selected base currency with its value.
     */
    var baseCurrency = Currency("EUR", 1.00, true)
    set(value) {
        field = value
        onBaseCurrencyChanged(value.key)
    }

    /**
     * The currently selected base currency's amount.
     */
    var baseCurrencyAmount: Double
    set(value) {
        baseCurrency.amount = value
        calculateCurrencyPrice()
    }
    get() = baseCurrency.amount

    private var latestResultRates: LatestRatesResponse? = null

    @VisibleForTesting (otherwise = VisibleForTesting.PRIVATE)
    internal var ratesUpdateSubscription = ArrayList<Subscription>()

    private val isAnimationShownEnough = AtomicBoolean(false)

    /**
     * Class initializer.
     */
    init {
        networkStateLiveData.postValue(NetworkState.LOADING)
    }

    /**
     * Notifies that ViewModel that the Activity is resumed.
     */
    fun onResume() {
        startCurrencyRatesUpdate()
        startTheMinimumLoadingTimerIfNeeded()
    }

    /**
     * Starts a timer that prevents the loading indicator/image to be hidden immediately in a very fast network.
     */
    private fun startTheMinimumLoadingTimerIfNeeded() {
        if (!isAnimationShownEnough.get()) {
            ratesUpdateSubscription.add(
                minimumLoadingImageTimerObservable
                    .subscribe {
                        if (isAnimationShownEnough.compareAndSet(false, true)) {
                            hideLoadingIndicatorIfPossible()
                        }
                    }
            )
        }
    }

    /**
     * Starts the observable that loads the latest rates from the server every {@link REQUEST_INTERVAL_IN_SECONDS}.
     */
    private fun startCurrencyRatesUpdate() {
        ratesUpdateSubscription.add(
            requestIntervalObservable
                .flatMap {
                    currencyRatesRepository
                        .getLatestRatesObservable(baseCurrency.key)
                        .onErrorReturn {
                            notifyUserForErrorIfNeeded()
                            null
                        }
                }
                .filter { t -> t != null }
                .doOnNext { it.rates[baseCurrency.key] = 1.00 }
                .subscribe { onRatesLoaded(it) })
    }

    /**
     * Notifies the ViewModel that the activity is paused.
     */
    fun onPause() {
        for (subscription in ratesUpdateSubscription) {
            if (!subscription.isUnsubscribed) {
                subscription.unsubscribe()
            }
        }

        ratesUpdateSubscription.clear()
    }

    private fun onBaseCurrencyChanged(newCurrency: String) {
        val oldList = ratesLiveData.value
        oldList?.let {
                rates ->
            for (currency in rates) {
                currency.isBase = currency.key == newCurrency
                currency.isEditable = currency.isBase
            }

            ratesLiveData.postValue(sortRatesList(rates))
        }
    }

    private fun onRatesLoaded(rates: LatestRatesResponse) {
        latestResultRates = rates
        calculateCurrencyPrice()
        hideLoadingIndicatorIfPossible()
    }

    private fun hideLoadingIndicatorIfPossible() {
        if (isAnimationShownEnough.get() && latestResultRates != null) {
            networkStateLiveData.postValue(NetworkState.SUCCESS)
        }
    }

    /**
     * Creates the list with the currencies based on the amount of the base currency and the latest data received
     * from the server.
     */
    private fun calculateCurrencyPrice() {
        val resultList = ArrayList<Currency>()
        if (latestResultRates != null && latestResultRates!!.rates[baseCurrency.key] != null) {
            val currency = latestResultRates!!.rates[baseCurrency.key]!!
            val recalculatedBaseCurrencyAmount = baseCurrency.amount / currency
            val entries = latestResultRates!!.rates.entries
            for (entry in entries) {
                val value = entry.value
                val key = entry.key
                resultList.add(Currency(key, recalculatedBaseCurrencyAmount * value, key == baseCurrency.key))
            }
        }

        ratesLiveData.postValue(sortRatesList(resultList))
    }

    /**
     * Sorts the list with {@link Currency} so the first item is the base currency and the rest
     * are sorted alphabetically.
     */
    private fun sortRatesList(resultList: ArrayList<Currency>): ArrayList<Currency> {
        resultList.sortWith(Comparator { item1, item2 ->
            when {
                item1.isBase -> -1
                item2.isBase -> 1
                else -> item1.key.compareTo(item2.key)
            }
        })

        return resultList
    }

    /**
     * If the previous state is NOT {@link NetworkState.SUCCESS} then we are going to send an error.
     * Otherwise we don't need as the user already has data that is displayed and we don't need to disturb his work.
     * The timer will trigger another request that will automatically retry the operation.
     */
    private fun notifyUserForErrorIfNeeded() {
        val lastValue = networkStateLiveData.value ?: NetworkState.LOADING
        if (lastValue != NetworkState.SUCCESS) {
            networkStateLiveData.postValue(NetworkState.ERROR)
        }
    }

    companion object {
        /**
         * The request time interval.
         */
        const val REQUEST_INTERVAL_IN_SECONDS = 1L

        /**
         * The minimum visible time in seconds of the loading indicator/image view.
         */
        const val MINIMUM_LOADING_INDICATOR_INTERVAL_IN_SECONDS = 2L
    }
}
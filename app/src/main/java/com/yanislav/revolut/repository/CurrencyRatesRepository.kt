package com.yanislav.revolut.repository

import com.yanislav.revolut.model.LatestRatesResponse
import com.yanislav.revolut.network.CurrencyRatesService
import com.yanislav.revolut.util.Mockable
import com.yanislav.revolut.util.RxSchedulers
import rx.Observable

/**
 * Repository class that provides the latest currency rates.
 */
interface CurrencyRatesRepository {
    fun getLatestRatesObservable(baseCurrency: String): Observable<LatestRatesResponse>
}

class CurrencyRatesRepositoryImpl(private val ratesService: CurrencyRatesService, private val rxSchedulers: RxSchedulers): CurrencyRatesRepository {

    override fun getLatestRatesObservable(baseCurrency: String): Observable<LatestRatesResponse> {
        return ratesService
            .getLatest(baseCurrency)
            .subscribeOn(rxSchedulers.io())
    }
}
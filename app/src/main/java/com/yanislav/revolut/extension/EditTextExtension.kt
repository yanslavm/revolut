package com.yanislav.revolut.extension

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.EditText


fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}

fun EditText.toDouble():Double {
    return try {
        text.toString().toDouble()
    } catch (e: NumberFormatException) {
        0.00
    }
}

fun EditText.showKeyBoard() {
    this.post {
        if (this.requestFocus()) {
            val imm = this.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            imm?.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
        }
    }
}
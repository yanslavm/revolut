package com.yanislav.revolut.extension

import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.yanislav.revolut.R

fun ImageView.load(currencyKey: String) {
    Picasso
        .get()
        .load("file:///android_asset/flags/${currencyKey.toLowerCase()}.png")
        .resizeDimen(R.dimen.adapter_item_image_size, R.dimen.adapter_item_image_size)
        .into(this)
}
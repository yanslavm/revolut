package com.yanislav.revolut.extension

fun Double.format(digits: Int) = java.lang.String.format("%.${digits}f", this)?: "0.00"
package com.yanislav.revolut.network

import com.yanislav.revolut.model.LatestRatesResponse
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable

interface CurrencyRatesService {
    @GET("latest")
    fun getLatest(@Query("base") baseCurrency: String): Observable<LatestRatesResponse>
}
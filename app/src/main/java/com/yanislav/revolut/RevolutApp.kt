package com.yanislav.revolut

import android.app.Application
import com.yanislav.revolut.di.*
import com.yanislav.revolut.util.Mockable
import org.koin.android.ext.android.startKoin

class RevolutApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule, mainActivityViewModule))
    }
}
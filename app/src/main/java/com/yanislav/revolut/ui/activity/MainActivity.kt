package com.yanislav.revolut.ui.activity

import android.animation.Animator
import android.graphics.drawable.Animatable
import android.os.Bundle
import android.view.View.*
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.yanislav.revolut.R
import com.yanislav.revolut.adapter.CurrenciesRatesAdapter
import com.yanislav.revolut.adapter.callback.CurrenciesRatesAdapterCallback
import com.yanislav.revolut.model.Currency
import com.yanislav.revolut.model.NetworkState
import com.yanislav.revolut.viewmodel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity(), CurrenciesRatesAdapterCallback {

    private val mainActivityViewModel: MainActivityViewModel by inject()

    private lateinit var adapter: CurrenciesRatesAdapter

    private val linearLayoutManager = LinearLayoutManager(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRatesRecyclerView()
        initViewModel()
    }

    private fun initRatesRecyclerView() {
        adapter = CurrenciesRatesAdapter(this)

        rates_list.layoutManager = linearLayoutManager
        rates_list.setHasFixedSize(true)
        rates_list.adapter = adapter
    }

    private fun initViewModel() {
        mainActivityViewModel.networkStateLiveData.observe(this, Observer { networkState ->
            when (networkState) {
                NetworkState.LOADING -> showLoadingImage()
                NetworkState.ERROR -> showLoadingImage()
                NetworkState.SUCCESS -> hideLoadingImage()
                null -> showMessage(R.string.network_error)
            }
        })

        mainActivityViewModel.ratesLiveData.observe(this, Observer { rates ->
            updateRates(rates)
        })
    }

    private fun hideLoadingImage() {
        if (loading_image.visibility == VISIBLE) {
            loading_image.visibility = GONE
            toolbar.visibility = VISIBLE
            rates_list.visibility = VISIBLE
        }
    }

    private fun showLoadingImage() {
        loading_image.visibility = VISIBLE
        rates_list.visibility = INVISIBLE

        val drawable = loading_image.drawable
        if (drawable is Animatable && !drawable.isRunning) {
            drawable.start()
        }
    }

    override fun onResume() {
        super.onResume()
        mainActivityViewModel.onResume()
    }

    override fun onPause() {
        super.onPause()
        mainActivityViewModel.onPause()
    }

    /**
     * Callback that is triggered when the user changes the amount of the current base currency.
     */
    override fun onCurrencyAmountChanged(amount: Double) {
        mainActivityViewModel.baseCurrencyAmount = amount
    }

    /**
     * Callback that is triggered when the user selects a new base currency.
     */
    override fun onCurrencyClicked(currency: Currency, position: Int) {
        linearLayoutManager.scrollToPosition(position)
        mainActivityViewModel.baseCurrency = currency
    }

    private fun showMessage(@StringRes messageStringResId: Int) {
        Snackbar.make(root_view, messageStringResId, Snackbar.LENGTH_LONG).show()
    }

    private fun updateRates(list: ArrayList<Currency>) {
        adapter.updateList(list)
    }
}

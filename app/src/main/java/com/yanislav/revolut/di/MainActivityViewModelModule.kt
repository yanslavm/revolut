package com.yanislav.revolut.di

import androidx.lifecycle.MutableLiveData
import com.yanislav.revolut.repository.CurrencyRatesRepository
import com.yanislav.revolut.repository.CurrencyRatesRepositoryImpl
import com.yanislav.revolut.viewmodel.MainActivityViewModel
import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import rx.Observable
import java.util.concurrent.TimeUnit

val mainActivityViewModule = module {
    single<CurrencyRatesRepository> {
        CurrencyRatesRepositoryImpl(get(), get())
    }

    viewModel { MainActivityViewModel(get(), MutableLiveData(), MutableLiveData(),
        Observable.interval(0, MainActivityViewModel.REQUEST_INTERVAL_IN_SECONDS, TimeUnit.SECONDS),
        Observable.timer(MainActivityViewModel.MINIMUM_LOADING_INDICATOR_INTERVAL_IN_SECONDS, TimeUnit.SECONDS)) }
}

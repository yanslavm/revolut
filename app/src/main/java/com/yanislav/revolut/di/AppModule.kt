package com.yanislav.revolut.di

import com.yanislav.revolut.network.CurrencyRatesService
import com.yanislav.revolut.util.DefaultRxScheduler
import com.yanislav.revolut.util.RxSchedulers
import okhttp3.OkHttpClient
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {
    single<OkHttpClient> {
        val okHttpClient = OkHttpClient.Builder()
        okHttpClient.build()
    }

    single<Retrofit> {
        Retrofit.Builder()
            .baseUrl("https://revolut.duckdns.org")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .client(get())
            .build()
    }

    single<CurrencyRatesService> {
        val retrofit: Retrofit = get()
        retrofit.create(CurrencyRatesService::class.java)
    }

    single<RxSchedulers> { DefaultRxScheduler() }
}
